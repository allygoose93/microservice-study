Project title - Microservice-study (conference go spin off)


Syncing data:
- to synchronize data from the monolith into the attendees microservice.
-add a lib to requirements.text .... in attendees_microservice/requirements.txt add
django-crontab==0.7.1
-write the sycn script in poll.py in attendees

-Add the script as a cron job, In the attendees_microservice/attendees_bc/settings.py, add the following code:
INSTALLED_APPS = [
    "django_crontab",
    ....
]

CRONJOBS = [
    ("* * * * *", "attendees.poll.get_conferences"),
]

Update the Dockerfile.dev in attendees microservice to install cron and include crontabs in the command line in the end

-make sure to run the docker run commands from the main directory!(in this case its microservice study)
